---
# You don't need to edit this file, it's empty on purpose.
# Edit theme's home layout instead if you wanna make some changes
# See: https://jekyllrb.com/docs/themes/#overriding-theme-defaults
layout: styleguide
---

# Couleurs

* orange (actif/sélection, important, rubrique, icônes, dates/métadonnées)
* noir
  * noir (textes, icônes)
  * gris souris (titres, couleur de fond alternative)
  * gris clair (placements de texte)
* blanc
  * blanc (titres alternatifs—fond non-blanc/sombre, texte des pictos/icônes)
  * blanc transparent (inserts)

# Typographie

* Verdana
  * Styles
    * Capitales (navigation—principale, titres—interface/rubriques)
    * Minuscules (texte, placement de texte, navigation)
  * Tailles
    * Grande (titres, navigation)
    * Normale (texte, synopsis—bloc avec titre)
    * Petit (dates/métadonnées)
  * Graisse
    * Gras (titres)
    * Normal
* Franklin Gothic Semi Bold
  * Styles
    * Minuscules
  * Tailles
    * Grande (titre—bloc media)
    * Normale (synopsis—bloc sans titre)
  * Graisse
    * Gras
