---
title: "Législatives 2017: Messahel appelle à une forte participation de la communauté nationale à l'étranger"
layout: post-video
date: 2017-03-14
category: National
image_url: assets/news/9a2f338f8d676cb21b423943918f7cfd_L.jpg
video_url: https://www.youtube.com/embed/EHVkSf_MnSE
video_duration: 1h52m
author: Philippe Bernard (Londres, correspondant)
---

Le ministre des Affaires maghrébines, de l'Union Africaine (UA) et de la Ligue des Etats arabes, Abdelkader Messahel a appelé à une forte participation de la communauté algérienne établie à l'étranger lors des législatives du 4 mai prochain.

Invité de l'émission "Hiwar Essaa" de la télévision nationale, M. Messahel a estimé que "la communauté algérienne établie à l'étranger compte d'énormes potentialités et a un rôle important à jouer dans le développement du pays", souhaitant la voir participer massivement lors des législatives du 4 mai prochain et "contribuer au processus de prise de décision".

Les préparatifs pour ce rendez-vous "se déroulent normalement au niveau des quatre zones (Maghreb arabe et Afrique, France nord/sud-Europe et Amérique), a indiqué le ministre relevant que les ambassades et consulats comptent près de deux millions d'inscrits.

> La nouveauté lors de ces élections, a-t-il ajouté, réside dans "la désignation de membres de la Haute instance indépendante de surveillance des élections (HIISE) pour ces zones".

Il a appelé à saisir cette occasion pour "renforcer le front interne" d'autant, a-t-il dit, que ces élections "interviennent dans un nouveau contexte à la lumière de la révision constitutionnelle et la consolidation de la démocratie dans le pays (...)".

M. Messahel a appelé en outre à "faire preuve de vigilance au vu des risques qui planent sur le pays" et à "être serein car l'Algérie est sur la bonne voie au plan politique, économique, culturel et même social".
