# EPTV Styleguide


![](MAQ-EPTV-2017.jpg)

```
jekyll server
```

# Documentations

* https://openclassrooms.com/
* https://openclassrooms.com/courses/comprendre-le-web
* https://openclassrooms.com/courses/apprenez-a-creer-votre-site-web-avec-html5-et-css3

* https://developer.mozilla.org : HTML, CSS et JavaScript
* http://styleguides.io : Catalogue et ressources de styleguides

* http://getbootstrap.com/components/#glyphicons : icônes de Bootstrap
* https://shopify.github.io/liquid/ : language de template de Jekyll
* https://jekyllrb.com/docs/templates/ : filtres et fonctions de Jekyll
* https://jekyllrb.com/docs/variables/ : variables disponibles dans Jekyll

* `site.posts` pour naviguer dans les posts
* `site.pages` pour naviguer dans les pages
* `site.components` pour naviguer dans les composants

# Icônes

* http://fontawesome.io/
* https://thenounproject.com/
* http://fontello.com/
* https://icomoon.io/

* https://fvsch.com/code/svg-icons/how-to/ Utilisation de SVG pour les icônes

# CSS

* [Dégradés](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Images/Using_CSS_gradients)
* [Animations](https://daneden.github.io/animate.css/)
* [Utiliser les animations](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Animations/Using_CSS_animations)
* [Unités de valeur](https://developer.mozilla.org/en-US/docs/Learn/CSS/Introduction_to_CSS/Values_and_units)

# Inspirations


## web

- http://ux.mailchimp.com/patterns
- https://www.lightningdesignsystem.com/
- http://style.maban.co.uk/ ([source](https://github.com/maban/styleguide))
- http://rizzo.lonelyplanet.com/styleguide/design-elements/colours ([source](https://github.com/lonelyplanet/rizzo))
- http://www.bbc.co.uk/gel

## Éditorial

- https://www.theguardian.com/guardian-observer-style-guide-a

# Interfaces web pour jekyll

* https://www.siteleaf.com/
* https://forestry.io/
